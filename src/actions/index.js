const booksRequested = () => ({
    type: 'BOOKS_REQUESTED'
})
const booksLoaded = (booksList) => ({
    type: 'BOOKS_SUCCESS',
    payload: booksList
})
const booksError = (errorMsg) => ({
    type: 'BOOKS_FAILED',
    payload: errorMsg
})

const fetchBooks = (dispatch, bookstoreService) => {
    dispatch(booksRequested())
    bookstoreService.getBooks().then(books => {
        dispatch(booksLoaded(books))
    }).catch(error => dispatch(booksError(error)))
}

const bookAddToCart = (bookId) => {
    return {
        type: 'BOOK_ADD_TO_CART',
        payload: bookId,
    }
}

const bookRemoveFromCart = (bookId) => {
    return {
        type: 'BOOK_REMOVE_FROM_CART',
        payload: bookId,
    }
}

const bookRemoveAllItemsFromCart = (bookId) => {
    return {
        type: 'BOOK_REMOVE_ALL_ITEMS_FROM_CART',
        payload: bookId,
    }
}

export {fetchBooks, bookAddToCart, bookRemoveFromCart, bookRemoveAllItemsFromCart}