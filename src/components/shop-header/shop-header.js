import React from 'react';
import './shop-header.css';
import { useSelector } from "react-redux";
import { Link } from 'react-router-dom';

const ShopHeader = () => {
    const items = useSelector((state) => state.cartItems);
    const itemsInCartCount = items.length;
    const totalPrice = useSelector((state) => state.total);
    return (
        <header className="shop-header row">
            <Link to="/">
                <div className="logo text-dark">ReStore</div>
            </Link>
            <Link to="/cart">
                <div className="shopping-cart">
                    <i className="cart-icon fa fa-shopping-cart" />
                    {itemsInCartCount} items (${totalPrice})
                </div>
            </Link>
        </header>
    );
};

export default ShopHeader;
