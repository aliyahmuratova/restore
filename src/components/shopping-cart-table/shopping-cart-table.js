import React from 'react';
import { connect } from 'react-redux';
import { bookAddToCart, bookRemoveFromCart, bookRemoveAllItemsFromCart } from "../../actions";
import './shopping-cart-table.css';

const ShoppingCartTable = ({cartItems, total, bookAddToCart, bookRemoveFromCart, bookRemoveAllItemsFromCart}) => {
  const booksInCart = cartItems.map((book, idx) => {
    return (
        <tr key={book.id}>
          <td>{idx + 1}</td>
          <td>{book.title}</td>
          <td>{book.count}</td>
          <td>${book.total}</td>
          <td>
            <button onClick={() => bookRemoveAllItemsFromCart(book.id)} className="btn btn-outline-danger btn-sm float-right">
              <i className="fa fa-trash-o" />
            </button>
            <button onClick={() => bookAddToCart(book.id)} className="btn btn-outline-success btn-sm float-right">
              <i className="fa fa-plus-circle" />
            </button>
            <button onClick={() => bookRemoveFromCart(book.id)} className="btn btn-outline-warning btn-sm float-right">
              <i className="fa fa-minus-circle" />
            </button>
          </td>
        </tr>
    )
  })

  return (
      <div className="shopping-cart-table">
        <h2>Your Order</h2>
        <table className="table">
          <thead>
          <tr>
            <th>#</th>
            <th>Item</th>
            <th>Count</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
          </thead>

          <tbody>
          {booksInCart}
          </tbody>
        </table>

        <div className="total">
          Total: ${total}.00
        </div>
      </div>
  );
};

const mapStateToProps = (state) => state

const mapDispatchToProps = (dispatch) => {
  return {
    bookAddToCart: (bookId) => dispatch(bookAddToCart(bookId)),
    bookRemoveFromCart: (bookId) => dispatch(bookRemoveFromCart(bookId)),
    bookRemoveAllItemsFromCart: (bookId) => dispatch(bookRemoveAllItemsFromCart(bookId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartTable);
