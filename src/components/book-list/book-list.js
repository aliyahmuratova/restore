import React, { useEffect } from 'react';
import BookListItem from '../book-list-item';
import { connect } from 'react-redux';
import { fetchBooks, bookAddToCart } from '../../actions';

import { withBookstoreService } from '../hoc';
import './book-list.css';


const BookList = ({bookstoreService, books, load, error, fetchBooks, bookAddToCart}) => {
  useEffect(() => fetchBooks(), [])

  return (
      <ul className="book-list">
        {
          books.map((book) => {
            return (
                <li key={book.id}><BookListItem bookAddToCart={bookAddToCart} book={book}/></li>
            )
          })
        }
      </ul>
  );
}

const mapStateToProps = (state) => {
  return state
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchBooks: () => fetchBooks(dispatch, ownProps.bookstoreService),
    bookAddToCart: (bookId) => dispatch(bookAddToCart(bookId)),
  }
}

export default withBookstoreService(connect(mapStateToProps, mapDispatchToProps)(BookList))

