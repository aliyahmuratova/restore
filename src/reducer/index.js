const updateCartItems = (cartItems, item, idx) => {

    if (item.count === 0) {
        return [
            ...cartItems.slice(0, idx),
            ...cartItems.slice(idx + 1)
        ];
    }

    if (idx === -1) {
        return [
            ...cartItems,
            item
        ];
    }

    return [
        ...cartItems.slice(0, idx),
        item,
        ...cartItems.slice(idx + 1)
    ];
};

const updateCartItem = (book, item = {}, quantity) => {

    const {
        id = book.id,
        count = 0,
        title = book.title,
        total = 0 } = item;

    return {
        id,
        title,
        count: count + quantity,
        total: total + quantity*book.price
    };
};


const updateOrder = (state, bookId, quantity) => {
    const { books, cartItems, total } = state;

    const book = books.find(({id}) => id === bookId);
    const itemIndex = cartItems.findIndex(({id}) => id === bookId);
    const item = cartItems[itemIndex];

    const newItem = updateCartItem(book, item, quantity);
    return {
        ...state,
        cartItems: updateCartItems(cartItems, newItem, itemIndex),
        total: total + (book.price * quantity )
    };
};

const defaultState = {
    books: [],
    load: false,
    error: false,
    cartItems: [],
    total: 0
}

const reducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'BOOKS_REQUESTED':
            return {
                ...state,
                load: true,
            }
        case 'BOOKS_SUCCESS':
            return {
                ...state,
                books: action.payload,
                load: false,
                error: false,
            }
        case 'BOOKS_FAILED':
            return {
                ...state,
                load: false,
                error: true,
            }
        case 'BOOK_ADD_TO_CART':
            return updateOrder(state, action.payload, 1)
        case 'BOOK_REMOVE_FROM_CART':
            return updateOrder(state, action.payload, -1)
        case 'BOOK_REMOVE_ALL_ITEMS_FROM_CART':
            const item = state.cartItems.find(({id}) => id === action.payload)
            return updateOrder(state, action.payload, -item.count)
        default:
            return state
    }
}

export default reducer;